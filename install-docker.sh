#!/bin/bash
# Install docker & docker-compose
sudo apt update -y
sudo apt upgrade -y
sudo apt install docker.io containerd docker-compose -y
sudo systemctl enable docker
sudo systemctl start docker
sudo usermod -aG docker $USER

# Setup datetime
sudo timedatectl set-timezone Asia/Jakarta
sudo timedatectl set-ntp on
